package be.digitalcity.projetfinal.controller;

import static java.nio.file.Files.copy;
import static java.nio.file.Paths.get;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import be.digitalcity.projetfinal.models.dto.PictureDTO;
import be.digitalcity.projetfinal.models.entity.TestObj;
import be.digitalcity.projetfinal.models.form.PictureForm;
import be.digitalcity.projetfinal.services.PictureService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(path = "picture")
public class PictureController {
    // Define the location for the files
    public static final String DIRECTORY = "src/main/resources/img/picture";

    private final PictureService service;

    @Autowired
    public PictureController(PictureService service) {
        this.service = service;
    }

    @GetMapping({""})
    public ResponseEntity<List<PictureDTO>> getAll(){
        return ResponseEntity.ok(service.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<PictureDTO> getOne(@PathVariable Long id){
        return ResponseEntity.ok(service.getOne(id));
    }

   @GetMapping(path = "/allBack", produces = MediaType.IMAGE_JPEG_VALUE)
   public ResponseEntity<TestObj> image() throws IOException {
       //final ByteArrayResource inputStream = new ByteArrayResource(Files.readAllBytes(Paths.get("src/main/resources/img/picture/8d638f7cad_50170753_22048-yuekai-du-grand-banquet-copie.jpg")));
       File img = new File("src/main/resources/img/picture/8d638f7cad_50170753_22048-yuekai-du-grand-banquet-copie.jpg");
       byte[] content = Files.readAllBytes(img.toPath());
        TestObj test = new TestObj();
        test.picture = Base64.getEncoder().encodeToString(content);
        return ResponseEntity.ok().body(test);
    //    return ResponseEntity.ok()
    //         .header("Content-Disposition", "attachment; filename=" +img.getName())
    //         .contentType(MediaType.valueOf(FileTypeMap.getDefaultFileTypeMap().getContentType(img)))
    //         .body(Files.readAllBytes(img.toPath()));
   }

    @GetMapping("/isAvailable")
    public ResponseEntity<List<PictureDTO>> getByAvailibility(){
        return ResponseEntity.ok(service.findByAvailability());
    }

    @PostMapping
    public ResponseEntity<PictureDTO> insert(@Valid @RequestBody PictureForm form){
        return ResponseEntity.ok(service.insert(form));
    }

    @PostMapping("/upload")
    public ResponseEntity<List<String>> uploadFile(@RequestParam("files") List<MultipartFile> multipartFiles) throws IOException {
        List<String> filenames = new ArrayList<>();
        for(MultipartFile file : multipartFiles) {
            String filename = StringUtils.cleanPath(file.getOriginalFilename());
            Path fileStorage = get(DIRECTORY, filename).toAbsolutePath().normalize();
            copy(file.getInputStream(), fileStorage, REPLACE_EXISTING);
            filenames.add(filename);
        }
        return ResponseEntity.ok().body(filenames);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<PictureDTO> delete(@PathVariable Long id){
        return ResponseEntity.ok(service.delete(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<PictureDTO> update(@PathVariable Long id, @Valid @RequestBody PictureForm form){
        return ResponseEntity.ok(service.update(id, form));
    }
}
